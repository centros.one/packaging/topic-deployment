package com.ddgix;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AlterConfigOp;
import org.apache.kafka.clients.admin.AlterConfigsResult;
import org.apache.kafka.clients.admin.Config;
import org.apache.kafka.clients.admin.ConfigEntry;
import org.apache.kafka.clients.admin.DescribeConfigsResult;
import org.apache.kafka.clients.admin.DescribeTopicsResult;
import org.apache.kafka.clients.admin.NewPartitions;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.admin.TopicDescription;
import org.apache.kafka.common.TopicPartitionInfo;
import org.apache.kafka.common.config.ConfigResource;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    private static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }

    private static String getRequiredEnv(String name) {
        String value = System.getenv(name);
        if (isNullOrEmpty(value)) {
            logger.error("Required environment variable " + name + " is not set.");
            System.exit(1);
        }
        return value;
    }

    private static String getOptionalEnv(String name, String defval) {
        String value = System.getenv(name);
        if (isNullOrEmpty(value)) {
            logger.info("Environment variable " + name + " is not set, using default value \"" + defval + "\"");
            value = defval;
        }
        return value;
    }

    public static void main(String[] args) {
        try {
            String bootstrapServer = getRequiredEnv("BOOTSTRAP_SERVER");
            String topicName = getRequiredEnv("TOPIC_NAME");

            Map<String, Object> clientConfig = new HashMap<>();
            clientConfig.put("bootstrap.servers", bootstrapServer);
            AdminClient client = AdminClient.create(clientConfig);

            Set<String> topics = null;
            try {
                topics = client.listTopics().names().get();
            } catch (InterruptedException | ExecutionException ex) {
                logger.error("Error retrieving topic list", ex);
                System.exit(1);
            }

            int partitions = 1;
            try {
                partitions = Integer.parseInt(getOptionalEnv("TOPIC_PARTITIONS", "1"));
            } catch (NumberFormatException ex) {
                logger.error("TOPIC_PARTITIONS is not a valid integer");
                System.exit(1);
            }

            short replicas = 1;
            try {
                replicas = Short.parseShort(getOptionalEnv("TOPIC_REPLICAS", "1"));
            } catch (NumberFormatException ex) {
                logger.error("TOPIC_REPLICAS is not a valid short integer");
                System.exit(1);
            }

            String configJson = getOptionalEnv("TOPIC_CONFIG", "{}");
            JSONObject configObject = new JSONObject(configJson);

            Map<String, String> topicConfig = configObject.toMap().entrySet().stream().collect(Collectors.toMap(
                    e -> e.getKey(), e -> e.getValue().toString()
            ));

            if (!topics.contains(topicName)) {
                NewTopic topic = new NewTopic(topicName, partitions, replicas);
                topic.configs(topicConfig);
                client.createTopics(Collections.singletonList(topic)).all().get();

                logger.info("Topic \"" + topicName + "\" created");
            } else {
                logger.info("Topic \"" + topicName + "\" already exists");

                TopicDescription description = client.describeTopics(Collections.singleton(topicName)).all().get().get(topicName);
                int currentPartitions = description.partitions().size();

                if (currentPartitions > partitions) {
                    logger.error("Partition count specified is " + partitions + ", but existing topic has "
                            + currentPartitions + " and reducing the partition count is not possible");
                    System.exit(1);
                }

                if (currentPartitions < partitions) {
                    logger.info("Increasing partition count from " + currentPartitions + " to " + partitions);
                    client.createPartitions(Collections.singletonMap(topicName, NewPartitions.increaseTo(partitions)));
                }

                for (TopicPartitionInfo tpi : description.partitions()) {
                    // TODO: change number of replicas (requires explicitly assigning them to individual brokers)
                    if (tpi.replicas().size() != replicas) {
                        logger.error("Replica count specified is " + replicas + ", but partition " + tpi.partition() + " of existing topic has " + tpi.replicas().size());
                        System.exit(1);
                    }
                }

                ConfigResource key = new ConfigResource(ConfigResource.Type.TOPIC, topicName);
                Config exisitingConfig = client.describeConfigs(Collections.singleton(key)).all().get().get(key);
                List<AlterConfigOp> alterations = new ArrayList<>();

                // TODO: how to handle resetting a value to default when the key is removed from the specified configuration?
                for (Map.Entry<String, String> item : topicConfig.entrySet()) {
                    String specval = item.getValue();
                    String currval = exisitingConfig.get(item.getKey()).value();
                    if (!Objects.equals(specval, currval)) {
                        alterations.add(new AlterConfigOp(new ConfigEntry(item.getKey(), specval), AlterConfigOp.OpType.SET));
                        logger.info("Changing config value for key '" + item.getKey() + "' from '" + currval + "' to '" + specval + "'");
                    }
                }

                if (!alterations.isEmpty()) {
                    client.incrementalAlterConfigs(Collections.singletonMap(key, alterations)).all().get();
                }
            }
        } catch (Exception ex) {
            logger.error("Topic deployment failed: " + ex.getMessage());
            System.exit(1);
        }
    }
}
