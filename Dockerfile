FROM maven:3.6.3-openjdk-11
WORKDIR /usr/src/build
COPY . .
RUN mvn package shade:shade -f pom.xml

FROM openjdk:11

RUN apt-get update \
    && apt-get install -y --no-install-recommends gosu \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

ARG COMMIT_SHA
ENV COMMIT_SHA ${COMMIT_SHA}
ARG COMMIT_REF_SLUG
ENV COMMIT_REF_SLUG ${COMMIT_REF_SLUG}

COPY --from=0 /usr/src/build/target/kafka-topic-deployment-shaded.jar .

ENTRYPOINT ["java", "-jar", "kafka-topic-deployment-shaded.jar"]
