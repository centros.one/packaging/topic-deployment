#!/usr/bin/env bash
set -euo pipefail

todo() {
  echo "TODO"
}

find-cluster-name() {
  # extract cluster name and (optionally) namespace from bootstrap server name
  if [[ $KAFKA_BOOTSTRAP_SERVER =~ ^([[:alnum:].-]*$CLUSTER_NAME_SUFFIX)(\.([[:alnum:]-]*))? ]]; then
    cluster_name=${BASH_REMATCH[1]}
    cluster_namespace=${BASH_REMATCH[3]}
    if [[ -z $cluster_namespace ]]; then
      # when a namespace is not part of the bootstrap server address, the cluster must be in the target namespace
      cluster_namespace=$NAMESPACE
    fi

    # look for a kafka instance with matching name+namespace
    instances=($(kubectl get sts -n "$cluster_namespace" -l=app="$KAFKA_K8S_APP" \
      -o=jsonpath="{range .items[*]}{.metadata.name}{'\t'}{end}"))

    for (( n = 0; n < ${#instances[@]}; n++ )); do
      # check if the cluster name matches the name extracted from the bootstrap server name
      if [[ ${instances[n]} == $cluster_name ]]; then
        # yes -> this is the cluster the bootstrap server belongs to
        return 0
      fi
    done
  fi

  echo "Unable to find kafka cluster for bootstrap-server $KAFKA_BOOTSTRAP_SERVER"
  exit 1
}

wait-for-cluster-ready() {
  sleeptime=10
  if [[ $CLUSTER_READY_TIMEOUT >  0 ]];  then
    retries=$((CLUSTER_READY_TIMEOUT/sleeptime))
  else
    retries=-1
  fi
 
  find-cluster-name
  echo "Kafka cluster name: $cluster_name / namespace: $cluster_namespace"

  while : ; do
    status=($(kubectl get sts --request-timeout="${KUBECTL_TIMEOUT}s" -n "$cluster_namespace" "$cluster_name" \
      -o=jsonpath='{.status.readyReplicas}{"\t"}{.status.replicas}'))

    ready=${status[0]}
    repl=${status[1]}
    echo "$ready/$repl nodes on cluster '$cluster_name' ready, tries left: $([[ $retries > 0 ]] && echo $retries || echo '∞')"
    if [[ $ready -eq $repl ]] || [[ $retries -eq 0 ]]; then
      break # done
    fi
    sleep $sleeptime
    if [[ $retries >  0 ]];  then
      ((retries--))
    fi
  done

  if [[ $retries ==  0 ]];  then
    echo "Error: cluster not ready, retries exhausted"
    exit 1
  fi
}

prepare-manifest() {
  export TOPIC_NAME=$1
  export IMAGE_REPOSITORY=$2
  export IMAGE_DIGEST=$3
  # other values for the template are already set as env vars by porter from the parameters
  gomplate -f k8s/job-template.yaml -o k8s/job.yaml
  if [[ $VERBOSE == "true" ]]; then
    cat k8s/job.yaml
  fi
}

wait-for-job-complete() {
  topic_name=$1
  all=($(kubectl get jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=topic-deployment -l=topic-name="$topic_name" \
    -o=jsonpath="{range .items[*]}{.metadata.name}{'\t'}{end}"))

  if [[ ${#all[@]} <  1 ]];  then
    echo "Error: job not found"
    exit 1
  fi

  sleeptime=2
  if [[ $JOB_COMPLETE_TIMEOUT >  0 ]];  then
    retries=$((JOB_COMPLETE_TIMEOUT/sleeptime))
  else
    retries=-1
  fi

  while : ; do
    active=($(kubectl get jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=topic-deployment -l=topic-name="$topic_name" \
      -o=jsonpath="{range .items[?(@.status.active==1)]}{.metadata.name}{'\t'}{end}"))

    echo "Waiting for job to complete, tries left: $([[ $retries >  0 ]] && echo $retries || echo '∞')"
    if [[ ${#active[@]} <  1 ]] || [[ $retries ==  0 ]];  then
      break # done
    fi
    sleep $sleeptime
    if [[ $retries >  0 ]];  then
      ((retries--))
    fi
  done

  if [[ $retries ==  0 ]];  then
    echo "Error: job did not complete, retries exhausted"
    exit 1
  fi

  # TODO: check job success/failure state & output log tail in case of failure
}

clean-jobs() {
  topic_name=$1
  all=$2
  extra_args=()
  if [[ $all != true ]]; then
    extra_args+=("--field-selector" "status.successful=1")
  fi
  kubectl delete jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=topic-deployment -l=topic-name="$topic_name" "${extra_args[@]}"
}

check-job-status() {
  topic_name=$1
  # successful jobs have been cleaned up; if any remain, they must have failed
  failed=($(kubectl get jobs --request-timeout="${KUBECTL_TIMEOUT}s" -n "$NAMESPACE" -l=k8s-app=topic-deployment -l=topic-name="$topic_name" \
      -o=jsonpath="{.items[*].metadata.name}"))
  if [[ ${#failed[@]} > 0 ]]; then
    echo "Error: job has failed"
    exit 1
  fi
}

# Call the requested function and pass the arguments as-is
"$@"
